export interface Quotes {
  symbol: string;
  openPrice: number;
  lastPrice: number;
  priceChangePercent: number;
}
