export interface TokenOperationData {
    asset: string,
    network: string,
    country?: string,
}
